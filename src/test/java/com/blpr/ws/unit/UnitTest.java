package com.blpr.ws.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.jvnet.staxex.util.FinalArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.blpr.ws.beans.Day;
import com.blpr.ws.beans.Month;
import com.blpr.ws.process.GetMonthInfo;
import com.blpr.ws.repositories.DayRepository;
import com.blpr.ws.repositories.MonthRepository;
import com.blpr.ws.soap.FinalProjectApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = FinalProjectApplication.class)
public class UnitTest {
	@Autowired
    private MonthRepository monthRepository;
    
    @Autowired
    private DayRepository dayRepository;
    
    private static final String FILE_NAME = "Registros_Momentum.xls";
    
    @Test 
    public void testFileReading() {
        System.out.println("Testea que el archivo este en la carpeta");
        File file = new File(FILE_NAME);
        assertTrue(file.exists());
    }
    
    @Test
    public void testGetAllLists() throws ParseException, IOException {
        //setup
        System.out.println("Testea que las listas no esten vacias obtenidas del excel");
        GetMonthInfo excelrepository = new GetMonthInfo();
        Object[] ret = excelrepository.setMonthInfo();
        List<Day> days = (List<Day>) ret[0];
        List<Month> meses = (List<Month>) ret[1];
        //verify
        assertNotNull(days);
        System.out.println(days.get(20).toString());
        assertNotNull(meses);
    }
    
    @Test
    public void findIsTestMonth() {
        //Setup
        System.out.println("Testea que cierto usuario este en la DB");
        Month expectedMonth = new Month();
        expectedMonth.setIsstr("CXMG");
        //execute
        Month currentMonth = monthRepository.findFirstByIsstr("CXMG");
        System.out.println(currentMonth.toString());
        //verify
        assertNotNull(currentMonth);
        assertEquals(currentMonth.getIsstr(), expectedMonth.getIsstr());
    }
    
    @Test 
    public void checkDay(){
        System.out.println("Testea que ya exista la base de datos DAY");
        //Setup
        //Execute
        List<Day> days = dayRepository.findAll();
        //Verify
        assertNotNull(days);
    }
    
	

}
