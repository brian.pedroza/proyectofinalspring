<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    <%@ page isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<%
	String contextPath = request.getContextPath();
%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<li><a href="<%= contextPath %>/api/v2/menu" >Back To Menu</a></li>
ALL RECORDS OF THE MONTH BY THE MONTH	
<table border="2" width="70%" cellpadding="2">
         <tr>
            <th>Id</th>
            <th>Is</th>
            <th>Mes</th>
            <th>Horas</th>
         </tr>
          <c:forEach items="${monthM}" var="v">
              <tr>
              	  <td>${v.id}</td>
                  <td>${v.isstr}</td>
                  <td>${v.imes}</td> 
                  <td>${v.hours}</td> 
               </tr>
          </c:forEach>
     </table>
</body>
</html>