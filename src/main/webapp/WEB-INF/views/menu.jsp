<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
	String contextPath = request.getContextPath();
%>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body >

<div align="justify">
	<h1>MENU: </h1>
	<li><a href="<%= contextPath %>/api/v2/days" >Records By All Days</a></li>
	<li><a href="<%= contextPath %>/api/v2/startAndEndDay">Records By Start And End Day</a></li>
	<li><a href="<%= contextPath %>/api/v2/isStartAndEndDay">Records By Is, Start And End Day</a></li>
	<li><a href="<%= contextPath %>/api/v2/month">Hours By Month</a></li>
	<li><a href="<%= contextPath %>/api/v2/isMonth">Record By Is And Month</a></li>
	<li><a href="<%= contextPath %>/api/v2/update">Data Proccess</a></li>
</div>

</body>
</html>