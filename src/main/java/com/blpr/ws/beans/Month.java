package com.blpr.ws.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Month {
	@Id
	@GeneratedValue
	private long id;
	
	private long imes;
	private String hours; //Cambiar a segundos
	private String isstr;
	
	
	
	public Month() {
		super();
	}
	
	public Month(long id, long imes, String hours, String isstr) {
		super();
		this.id = id;
		this.imes = imes;
		this.hours = hours;
		this.isstr = isstr;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getImes() {
		return imes;
	}
	public void setImes(long imes) {
		this.imes = imes;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getIsstr() {
		return isstr;
	}
	public void setIsstr(String isstr) {
		this.isstr = isstr;
	}
	
	@Override
	public String toString() {
		return "Month [id=" + id + ", imes=" + imes + ", hours=" + hours + ", isstr=" + isstr + "]";
	}
	
	
	
}
