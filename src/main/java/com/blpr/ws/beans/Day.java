package com.blpr.ws.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Day {
    @Id
    @GeneratedValue
    private long id;
    
    private String isstr;
    
    private String datestr;
    private String cin;
    private String cout;
    private String hours;
    
    
    public Day() {
        super();
    }
    public Day(long id, String isstr, String datestr, String cin, String cout, String hours) {
        super();
        this.id = id;
        this.isstr = isstr;
        this.datestr = datestr;
        this.cin = cin;
        this.cout = cout;
        this.hours = hours;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getIsstr() {
        return isstr;
    }
    public void setIsstr(String isstr) {
        this.isstr = isstr;
    }
    public String getDatestr() {
        return datestr;
    }
    public void setDatestr(String datestr) {
        this.datestr = datestr;
    }
    public String getCin() {
        return cin;
    }
    public void setCin(String cin) {
        this.cin = cin;
    }
    public String getCout() {
        return cout;
    }
    public void setCout(String cout) {
        this.cout = cout;
    }
    public String getHours() {
        return hours;
    }
    public void setHours(String hours) {
        this.hours = hours;
    }
    @Override
    public String toString() {
        return "Day [id=" + id + ", isstr=" + isstr + ", datestr=" + datestr + ", cin=" + cin + ", cout=" + cout + ", hours="
                + hours + "]";
    }
    
}
