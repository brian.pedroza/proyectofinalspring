package com.blpr.ws.services;

import java.util.List;

import com.blpr.ws.beans.Month;
import com.blpr.ws.beans.Response;

public interface MonthService {
	Response getAllHoursByIsAndMonth(String isstr, Long month);
	Response getAllUsersByMonth(Long month);
}
