package com.blpr.ws.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.blpr.ws.beans.Day;
import com.blpr.ws.beans.Month;

public class GetMonthInfo {
	
    private static final String FILE_NAME = "Registros_Momentum.xls";

	List<Day> listaDias = new ArrayList<>();
	List<Month> listaMeses = new ArrayList<>();
	Day objetoAuxiliarDia = new Day();
	Date currentDate;
	Date hour;
	int imes,iNewMes;
	long currentSeconds;
	long sumSeconds;
	long auxSegundos;
	String[] arrayTime;
	String Hours;
    String auxIs;



	
	public Object[] setMonthInfo() throws ParseException, IOException{
		GetDayInfo info = new GetDayInfo();
		listaDias = info.getAllDays();
		

		Iterator<Day> iteradorDias = listaDias.iterator();
		objetoAuxiliarDia = iteradorDias.next();
		while(iteradorDias.hasNext()) {
			Month objetoAuxiliarMes = new Month();
		    objetoAuxiliarMes.setIsstr(objetoAuxiliarDia.getIsstr());//Month.Is
		    auxIs = objetoAuxiliarDia.getIsstr();
		    currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(objetoAuxiliarDia.getDatestr());
		    imes = currentDate.getMonth() + 1 ;
		    sumSeconds = 0;
		    auxSegundos=0;
	    	arrayTime = objetoAuxiliarDia.getHours().split(":");
	    	sumSeconds += (Integer.parseInt(arrayTime[0])*3600) + (Integer.parseInt(arrayTime[1])*60) + (Integer.parseInt(arrayTime[2]));
			objetoAuxiliarDia = iteradorDias.next();
		    currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(objetoAuxiliarDia.getDatestr());
		    iNewMes = currentDate.getMonth() + 1 ;
		    
			
		    while(iNewMes == imes && auxIs.equals(objetoAuxiliarDia.getIsstr())) {
        		arrayTime = objetoAuxiliarDia.getHours().split(":");
    	    	sumSeconds += (Integer.parseInt(arrayTime[0])*3600) + (Integer.parseInt(arrayTime[1])*60) + (Integer.parseInt(arrayTime[2]));
		    	auxSegundos = (Integer.parseInt(arrayTime[0])*3600) + (Integer.parseInt(arrayTime[1])*60) + (Integer.parseInt(arrayTime[2]));
			    if(!iteradorDias.hasNext()) {
			    		break;
			    }else {
			    	objetoAuxiliarDia = iteradorDias.next(); //Aqui ya cambia de objeto
			    	currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(objetoAuxiliarDia.getDatestr());
			    	iNewMes = currentDate.getMonth() + 1 ;
			    	}
		    }
		    objetoAuxiliarMes.setImes(imes);
            
            if((sumSeconds/3600)<10) 
            	if(((sumSeconds%3600)/60)<10) 
            		if(((sumSeconds%3600)%60)<10)
            			Hours= "0"+(sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            		else Hours= "0"+(sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            	else if(((sumSeconds%3600)%60)<10) 
            				Hours= "0"+(sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            			else Hours= "0"+(sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            else if(((sumSeconds%3600)/60)<10)
            		if(((sumSeconds%3600)%60)<10) Hours= (sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            		else Hours= (sumSeconds/3600)+":0"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            	else if(((sumSeconds%3600)%60)<10) Hours= (sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":0"+((sumSeconds%3600)%60);
            		else Hours= (sumSeconds/3600)+":"+((sumSeconds%3600)/60)+":"+((sumSeconds%3600)%60);
            	
		    objetoAuxiliarMes.setHours(Hours);
		    listaMeses.add(objetoAuxiliarMes);

		}

		
		SetMonthInfoExcel();
		
		Object[] ret = {listaDias, listaMeses};
		return ret;
		
	}
	
	public void SetMonthInfoExcel() throws IOException {
		if(listaMeses.isEmpty()) { System.out.println("La lista esta vacia");}else {System.out.println("La lista no esta vacia");}
	
		try {
		
		
		//Blank workbook
        HSSFWorkbook workbook = new HSSFWorkbook(); 
         
        //Create a blank sheet
        Sheet sheet = workbook.createSheet("Employee Data");
		
        
        Row headerRow;
        Cell headerCell;
		headerRow = sheet.createRow(0);
		headerCell = headerRow.createCell(0);
		headerCell.setCellValue("Name");
		headerCell = headerRow.createCell(1);
		headerCell.setCellValue("Month");
		headerCell = headerRow.createCell(2);
		headerCell.setCellValue("Month Time");
		
		int i = 1;
		for(Month m : listaMeses) {
	        Row currentRow;
	        
			currentRow = sheet.createRow(i);
			
			Cell currentCell = currentRow.createCell(0);
			currentCell.setCellValue(m.getIsstr());
			
			currentCell = currentRow.createCell(1);
			currentCell.setCellValue(m.getImes());
			
			currentCell = currentRow.createCell(2);
			currentCell.setCellValue(m.getHours());//Si quieres obtener horas dividir entre 3600
			
			i++;
			
			}
		
			
        
        FileOutputStream out = new FileOutputStream(new File("Reporte.xls"));
        workbook.write(out);
        out.close();

        
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
		
	}
}
