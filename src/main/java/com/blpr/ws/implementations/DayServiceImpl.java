package com.blpr.ws.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blpr.ws.beans.Day;
import com.blpr.ws.beans.Response;
import com.blpr.ws.repositories.DayRepository;
import com.blpr.ws.services.DayService;
import com.fasterxml.jackson.databind.util.JSONPObject;

@Service
public class DayServiceImpl implements DayService {

	@Autowired
	private DayRepository dayRepository;
	


	@Override
	public Response getAllBySatartAndEndDate(String startDate, String endDate) {
		List<Day> d = dayRepository.findAllByDatestrBetween(startDate, endDate);
		Response res = new Response();
		if(!d.isEmpty()) { res.setResult(d); res.setTypeResponse("200 Succefully");}
		else { res.setResult(null); res.setTypeResponse("404 Not Found");}
		return res;
	}

	@Override
	public Response getAllRecordsBySatartAndEndDateAndIs(String is, String startDate, String endDate) {
		List<Day> d = dayRepository.findAllByIsstrAndDatestrBetween(is,startDate,endDate);
		Response res = new Response();
		if(!d.isEmpty()) { res.setResult(d); res.setTypeResponse("200 Succefully");}
		else { res.setResult(null); res.setTypeResponse("404 Not Found");}
		return res;
	}

	@Override
	public Response getAll() {
		List<Day> d = dayRepository.findAll();
		Response res = new Response();
		if(!d.isEmpty()) { res.setResult(d); res.setTypeResponse("200 Succefully");}
		else { res.setResult(null); res.setTypeResponse("404 Not Found");}
		return res;
	}
	
	
}
