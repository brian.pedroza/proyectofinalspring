package com.blpr.ws.implementations;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blpr.ws.beans.Day;
import com.blpr.ws.beans.Month;
import com.blpr.ws.beans.Response;
import com.blpr.ws.process.GetMonthInfo;
import com.blpr.ws.repositories.DayRepository;
import com.blpr.ws.repositories.MonthRepository;
import com.blpr.ws.services.UpdateService;

@Service
public class UpdateServiceImpl implements UpdateService{

	@Autowired
	private DayRepository dayRepository;
	
	@Autowired
	private MonthRepository monthRepository;
	
	/*public UpdateServiceImpl(MonthRepository monthRepository) {
		this.monthRepository = monthRepository;
	}*/
	
	
	@Override
	public Response updateAll() throws ParseException, IOException {
    	GetMonthInfo info = new GetMonthInfo();
    	System.out.println("Crea objeto");
    	Object[] obj = info.setMonthInfo();
    	List<Day> days = (List<Day>) obj[0];
    	List<Month> months = (List<Month>) obj[1];
    	Response res = new Response();
    	if(obj.length>0) {
    		res.setResult("Congratulations on updating the database");
    		res.setTypeResponse("200 Succefully");
    	}
    	else {
    		res.setResult("There was a mistake");
    		res.setTypeResponse("500 ");
    	}
    	dayRepository.deleteAll();
    	System.out.println("COUNT"+dayRepository.count());
    	if(dayRepository.count()>0) {
    		dayRepository.deleteAllInBatch();
    	}
    	dayRepository.saveAll(days);
    	monthRepository.deleteAll();
    	System.out.println("COUNT"+monthRepository.count());
    	if(monthRepository.count()>0) {
    		monthRepository.deleteAllInBatch();
    	}
    	monthRepository.saveAll(months);
		return res;
	}

}
